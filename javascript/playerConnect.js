function refreshGamesEachSecond() {
    setInterval(refreshGames, 1000);
}

function refreshGames() {
    rpsApi.allGames()
        .then(games => {
            if (games.length === 0) {
                document.getElementById('gaming').innerHTML = 'Inga tillgängliga spel hittades'
            }
            let gamesListHtml = games.map(game => {
                return createGameRow(game.gameId, game.name)
            })
                .join('');
            document.getElementById('gaming').innerHTML = gamesListHtml;
        })
}

function createGameRow(gameId, name) {
    if (name == null) {
        name = 'nameless game'
    }
    return `<li class="d-flex flex-row justify-content-around align-items-center">
    <button id="nameButton" class="btn btn-love btn-block text-white" onclick="joinGame('${gameId}')"> ${name} </button></li>`
}

function joinGame(gameId) {
    rpsApi.joinGame(gameId)
        .then(gameStatus => {
            window.location.href = "../html/create-game.html";
        })
}

function newGameRemote() {
    rpsApi.newGame()
        .then(() => window.location.href = 'create-game.html');
}


function getName() {
    let playerName = document.getElementById('userName').value;
    console.log(playerName)
    rpsApi.setName(playerName)
    console.log(playerName)
    document.getElementById('computer-label').innerHTML = game.opponentName;
}

refreshGamesEachSecond();
