const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");
const hitSound = new Audio('../sounds/swish.m4a');
const winSound = new Audio('../sounds/cash.mp3');
const lossSound = new Audio('../sounds/aww.mp3');

function convertToWord(letter) {
    if (letter === "ROCK") return "Rock";
    if (letter === "PAPER") return "Paper";
    if (letter === "SCISSORS") return "Scissors";
}

function convertToId(letter) {
    if (letter === "ROCK") return "r";
    if (letter === "PAPER") return "p";
    if (letter === "SCISSORS") return "s";
}

function win(userChoice, computerChoice) {
    const userChoice_div = document.getElementById(convertToId(userChoice));
    winSound.play();
    result_p.innerHTML = `${convertToWord(userChoice)} beats ${convertToWord(computerChoice)}
    . You Win!`;
    setTimeout(function () {
    }, 300);
}

function lose(userChoice, computerChoice) {
    const userChoice_div = document.getElementById(convertToId(userChoice));
    lossSound.play();
    result_p.innerHTML = `${convertToWord(userChoice)} loses to ${convertToWord(computerChoice)}
    . You lost...`;
    setTimeout(function () {
    }, 300);
}

function tie(userChoice, computerChoice) {
    const userChoice_div = document.getElementById(convertToId(userChoice));
    hitSound.play();
    result_p.innerHTML = `${convertToWord(userChoice)} equals to ${convertToWord(computerChoice)}
    . It's a Tie!`;
    setTimeout(function () {
    }, 300);
}

function game(userChoice) {
    rpsApi.userMove(userChoice)
        .then(gameStatus => {
            console.log('player move result', gameStatus);
            switch (gameStatus.game) {
                case 'WIN':
                    win(gameStatus.move, gameStatus.opponentMove);
                    break;
                case 'LOSE':
                    lose(gameStatus.move, gameStatus.opponentMove);
                    break;
                case 'DRAW':
                    tie(gameStatus.move, gameStatus.opponentMove);
                    break;
            }
        })
}


function main() {
    rock_div.addEventListener('click', function () {
        game("ROCK")
    })
    paper_div.addEventListener('click', function () {
        game("PAPER")
    })
    scissors_div.addEventListener('click', function () {
        game("SCISSORS")
    })
}

function startGame() {
    rpsApi.newGame().then(gameStatus => {
        window.location.href = 'startgame.html';
        let name = document.getElementById('userName').value;
        rpsApi.setName(name);
    })
}

function refreshGamesEverySec() {
    setInterval(gameRefresh, 1000);
}

function gameRefresh() {
    rpsApi.gameStatus()
        .then(game => {
            console.log('game id :' + game.gameId)
            document.getElementById('user-label').innerHTML = game.name;
            console.log('spelare val :' + game.move)
            console.log('SpelLäge :' + game.game)
            document.getElementById('computer-label').innerHTML = game.opponentName;
            console.log("Motspelalens val:" + game.opponentMove)
            console.log(game.opponentName)
            console.log(game.name)
        })
}

refreshGamesEverySec();
main();