let userScore = 0;
let computerScore = 0;

const userScore_span = document.getElementById("user-score");
const computerScore_span = document.getElementById("computer-score");
const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById("r");
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");
const hitSound = new Audio('../sounds/swish.m4a');
const winSound = new Audio('../sounds/cash.mp3');
const lossSound = new Audio('../sounds/aww.mp3');

function getComputerChoice() {
    const choices = ['r', 'p', 's'];
    const randomNumber = (Math.floor(Math.random() * 3));
    return choices[randomNumber];
}

function convertToWord(letter) {
    if (letter === "r") return "Rock";
    if (letter === "p") return "Paper";
    if (letter === "s") return "Scissors";
}

function win(userChoice, computerChoice) {
    const smallUserWord = "user".fontsize(3).sup();
    const smallUCompWord = "comp".fontsize(3).sup();
    const userChoice_div = document.getElementById(userChoice);
    userScore++;
    winSound.play();
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} beats ${convertToWord(computerChoice)}${smallUCompWord}. User Win!`;
    userChoice_div.classList.add('green-glow');
    setTimeout(function () {
        userChoice_div.classList.remove('green-glow')
    }, 300);
}

function lose(userChoice, computerChoice) {
    const smallUserWord = "user".fontsize(3).sup();
    const smallUCompWord = "comp".fontsize(3).sup();
    const userChoice_div = document.getElementById(userChoice);
    computerScore++;
    lossSound.play();
    userScore_span.innerHTML = userScore;
    computerScore_span.innerHTML = computerScore;
    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} loses to ${convertToWord(computerChoice)}${smallUCompWord}. You lost...`;
    userChoice_div.classList.add('red-glow');
    setTimeout(function () {
        userChoice_div.classList.remove('red-glow')
    }, 300);
}

function tie(userChoice, computerChoice) {
    const smallUserWord = "user".fontsize(3).sup();
    const smallUCompWord = "comp".fontsize(3).sup();
    const userChoice_div = document.getElementById(userChoice);
    hitSound.play();
    result_p.innerHTML = `${convertToWord(userChoice)}${smallUserWord} equals to ${convertToWord(computerChoice)}${smallUCompWord}. It's a Tie!`;
    userChoice_div.classList.add('gray-glow');
    setTimeout(function () {
        userChoice_div.classList.remove('gray-glow')
    }, 300);
}


function game(userChoice, userScore, computerScore) {
    const computerChoice = getComputerChoice();
    if (userChoice === computerChoice) {
        tie(userChoice, computerChoice)
        return;
    }
    if (userChoice === "r") {
        if (computerChoice === "s") {
            win(userChoice, computerChoice)
            return;
        }
    }
    if (userChoice === "s") {
        if (computerChoice === "p") {
            win(userChoice, computerChoice)
            return;
        }
    }
    if (userChoice === "p") {
        if (computerChoice === "r") {
            win(userChoice, computerChoice)
        }
    } else {
        computerScore++;
        lose(userChoice, computerChoice)
    }
}

function main() {
    rock_div.addEventListener('click', function () {
        game("r");
    })
    paper_div.addEventListener('click', function () {
        game("p");
    })
    scissors_div.addEventListener('click', function () {
        game("s");
    })
}


main();
