FROM nginx

RUN mkdir /usr/share/nginx/html/css
RUN mkdir /usr/share/nginx/html/html
RUN mkdir /usr/share/nginx/html/javascript
RUN mkdir /usr/share/nginx/html/sounds
RUN mkdir /usr/share/nginx/html/style

COPY css/* /usr/share/nginx/html/css
COPY html/* /usr/share/nginx/html/html
COPY javascript/* /usr/share/nginx/html/javascript
COPY sounds/* /usr/share/nginx/html/sounds
COPY style/* /usr/share/nginx/html/style

COPY index.html /usr/share/nginx/html/